# CV generation
# Copyright (C) 2022 Yannick Ulrich
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from jinja2 import Template
import re
import yaml


def latex_replace(s):
    return s.replace('\\\\', '\\')\
            .replace('&#A;', '\\\\')\
            .replace('&uuml;', '\\"{u}')\
            .replace('&Uuml;', '\\"{U}')\
            .replace('&ouml;', '\\"{o}')\
            .replace('&Ouml;', '\\"{O}')\
            .replace('&oslash;', '\\o{}')\
            .replace('&pound;', '$\\pounds$')


def get_latex_template(fn):
    with open(fn) as fp:
        tmpl = fp.read()

    return latex_replace, Template(
        tmpl,
        block_start_string=r'\BLOCK{',
        block_end_string='}',
        variable_start_string=r'\VAR{',
        variable_end_string='}',
        comment_start_string=r'\#{',
        comment_end_string='}',
        line_statement_prefix='%%',
        line_comment_prefix='%#',
        trim_blocks=True,
        autoescape=False
    )


def html_replace(s):
    def nth(n):
        n = int(n)
        match n % 10:
            case 0:
                suf = 'th'
            case 1:
                suf = 'st'
            case 2:
                suf = 'nd'
            case 3:
                suf = 'rd'
            case _:
                suf = 'th'
        return f'{n}<sup>{suf}</sup>'

    s = re.sub(r'\\nth{(\d*)}', lambda x: nth(x.group(1)), s)
    s = re.sub(r'{\\sc ([^}]*)}', r'<span class="sc">\1</span>', s)
    s = re.sub(r'\`(.*?)\'', '\u2018\\1\u2019', s)
    return s.replace('--', '&mdash;')\
            .replace('&#A;', '<br>')\
            .replace('\\LaTeX', '$\\LaTeX$')


def get_html_template(fn):
    with open(fn) as fp:
        tmpl = fp.read()

    return html_replace, Template(
        tmpl,
        trim_blocks=True,
        lstrip_blocks=True
    )


def render(yml, tmpl, getter, output):
    with open(yml) as fp:
        cv = yaml.safe_load(fp.read())

    r, template = getter(tmpl)
    with open(output, 'w') as fp:
        fp.write(r(template.render(cv=cv, len=len, enumerate=enumerate)))


if __name__ == '__main__':
    render('cv.yaml', 'template.tex.jinja2', get_latex_template, 'output.tex')
    render('cv.yaml', 'publication_template.tex.jinja2', get_latex_template, 'list-of-publications.tex')
    render('cv.yaml', 'template.html.jinja2', get_html_template, 'output.html')
