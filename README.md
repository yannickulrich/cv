# My academic CV

This creates both a [CV website](https://yannickulrich.gitlab.io/cv/) and a [pdf](https://yannickulrich.gitlab.io/cv/cv.pdf) from the same [YAML file](https://gitlab.com/yannickulrich/cv/-/blob/root/cv.yaml) using Jinja templates.

The code and templates for this are licenced under [GPLv3](https://gitlab.com/yannickulrich/cv/-/blob/root/LICENCE) as are the examples.
The content of my CV is licenced under [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) meaning that you may not modify it.

## How it works
The YAML file has the following format
```yaml
meta:
  firstname: <first name>
  lastname: <last name>
  address:
    - <First address line>
    - <Second address line>
    - ...
    - <Last address line>
  phone: <phone number>
  email: <email address>
  orcid: <orcid number>

sections:
  - name: <Section 1>
    content:
      - - <First item, category>
        - <First item, first part>
        - <First item, second part>
        - ...
        - <First item, last part>
      - - <Second item, category>
        - <Second item, first part>
        - <Second item, second part>
        - ...
        - <Second item, last part>
  - ...
```
Items may have four parts which translates to the `moderncv` format
```latex
\cvitem{<category>}{}
\cvitem{<category>}{<first part>}
\cventry{<category>}{<first part>}{<second part>}{}{}{}
\cventry{<category>}{<first part>}{<second part>}{<third part>}{}{}
\cventry{<category>}{<first part>}{<second part>}{<third part>}{}{<fourth part>}
```
Every section of the file may include LaTeX which is rendered using MathJAX.

### Special sections
There are two types of special sections: publications and talks.

#### Publications
If a section has `citekeys` instead of `content` part, it's treated as a publication section.
It contains a list of maps with the following keys
 * `key`: BibTeX cite key
 * `author`: list of authors (should already be in final format)
 * `title`: title of the paper
 * `date`: year of publication
 * `journal`, `volume`, `page` (optional): bibliographic information for published works
 * either one of:
    * `arxiv`: arXiv number
    * `doi`: DOI reference for the work
    * `url`: URL to the work
 * `in`: if published as part of something


#### Talks
If a section has `talks` instead of `content` part, it's treated as a list of talks.
It contains a list of maps with the following keys
 * `date`: date of the talk
 * `title`: title of the talk
 * `series`: name of conference or seminar series
 * `place` (optional): place the talk was given at
 * `link` (optional): link to conference or seminar website
 * `pdf` (optional): link to slides
